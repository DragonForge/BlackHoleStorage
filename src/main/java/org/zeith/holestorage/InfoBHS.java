package org.zeith.holestorage;

import com.zeitheron.hammercore.internal.blocks.base.IBlockOrientable;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public class InfoBHS
{
	public static final String MOD_ID = "blackholestorage",
			MOD_NAME = "Black Hole Storage",
			MOD_VERSION = "@VERSION@";

	public static final String PROXY_BASE = "org.zeith.holestorage.proxy",
			PROXY_CLIENT = PROXY_BASE + ".ClientProxy",
			PROXY_SERVER = PROXY_BASE + ".CommonProxy";

	public static final PropertyEnum<EnumFacing> FACING_UD = PropertyEnum.create("facing", EnumFacing.class, EnumFacing.UP, EnumFacing.DOWN);
	public static final PropertyEnum<EnumFacing> FACING_UDEWSN = IBlockOrientable.FACING;

	public static ResourceLocation texture(String p)
	{
		return new ResourceLocation(MOD_ID, "textures/" + p);
	}
}