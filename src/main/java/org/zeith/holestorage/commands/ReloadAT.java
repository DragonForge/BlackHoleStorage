package org.zeith.holestorage.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import org.zeith.holestorage.api.atomictransformer.RecipesAtomicTransformer;

public class ReloadAT
		extends CommandBase
{
	@Override
	public String getName()
	{
		return "reload_at";
	}

	@Override
	public String getUsage(ICommandSender sender)
	{
		return "Reloads Atomic Transformer recipes and syncs to clients.";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		RecipesAtomicTransformer.reload();
		sender.sendMessage(new TextComponentString("Recipes have been updated. Syncing " + RecipesAtomicTransformer.getRecipes().size() + " recipes to clients."));
	}
}