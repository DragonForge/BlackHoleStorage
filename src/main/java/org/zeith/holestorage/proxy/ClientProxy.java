package org.zeith.holestorage.proxy;

import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.zeith.holestorage.InfoBHS;
import org.zeith.holestorage.api.atomictransformer.RecipesAtomicTransformer;
import org.zeith.holestorage.client.Render3D;
import org.zeith.holestorage.client.particle.ParticleEnergyFX;
import org.zeith.holestorage.client.particle.ParticleLiquid;
import org.zeith.holestorage.client.particle.ParticleLiquidTextured;
import org.zeith.holestorage.client.render.item.ItemRenderAntiMatter;
import org.zeith.holestorage.client.tesr.*;
import org.zeith.holestorage.configs.BHSConfigs;
import org.zeith.holestorage.init.ItemsBHS;
import org.zeith.holestorage.tile.*;
import org.zeith.holestorage.vortex.Vortex;

import java.util.HashSet;
import java.util.Set;

public class ClientProxy
		extends CommonProxy
{
	public static boolean rendering = false;
	public static Entity renderEntity = null;
	public static Entity backupEntity = null;

	public static Set<Vortex> particleVortex = new HashSet<>();

	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileBlackHoleFormer.class, new TileRenderBlackHoleFormer());
		ClientRegistry.bindTileEntitySpecialRenderer(TileAtomicTransformer.class, new TileRenderAtomicTransformer());
		{
			TileRenderWormhole w;
			ClientRegistry.bindTileEntitySpecialRenderer(TileWormhole.class, w = new TileRenderWormhole());
			MinecraftForge.EVENT_BUS.register(w);
		}
		ClientRegistry.bindTileEntitySpecialRenderer(TileWormholeFormer.class, new TileRenderWormholeFormer());
		ClientRegistry.bindTileEntitySpecialRenderer(TileBlackHole.class, new TileRenderBlackHole());
	}

	@SubscribeEvent
	public void onClientWorldLoad(WorldEvent.Load event)
	{
		if(event.getWorld() instanceof WorldClient)
			TileRenderWormhole.wormholeGlobalRenderer.setWorldAndLoadRenderers((WorldClient) event.getWorld());
	}

	@SubscribeEvent
	public void onClientWorldUnload(WorldEvent.Unload event)
	{
		if(event.getWorld() instanceof WorldClient)
			TileRenderWormhole.clearRegisteredWormholes();
	}

	@SubscribeEvent
	public void onPrePlayerRender(RenderPlayerEvent.Pre event)
	{
		if(!rendering)
			return;

		if(event.getEntityPlayer() == renderEntity)
		{
			backupEntity = Minecraft.getMinecraft().getRenderManager().renderViewEntity;
			Minecraft.getMinecraft().getRenderManager().renderViewEntity = renderEntity;
		}
	}

	@SubscribeEvent
	public void onPostPlayerRender(RenderPlayerEvent.Post event)
	{
		if(!rendering)
			return;

		if(event.getEntityPlayer() == renderEntity)
		{
			Minecraft.getMinecraft().getRenderManager().renderViewEntity = backupEntity;
			renderEntity = null;
		}
	}

	@SubscribeEvent
	public void onClientTick(TickEvent.ClientTickEvent e)
	{
		if(e.phase == TickEvent.Phase.END && Minecraft.getMinecraft().world == null)
		{
			RecipesAtomicTransformer.RECIPES.get(Side.CLIENT).clear();
			BHSConfigs.resetClient();
		}
	}

	@Override
	public void preInit()
	{
		OBJLoader.INSTANCE.addDomain(InfoBHS.MOD_ID);
		MinecraftForge.EVENT_BUS.register(new Render3D());
		ItemRenderingHandler.INSTANCE.setItemRender(ItemsBHS.ANTI_MATTER, new ItemRenderAntiMatter());
	}

	@Override
	public void spawnEnergyFX(World w, double x, double y, double z, double tx, double ty, double tz, int rf)
	{
		if(w == null)
			return;
		if(w.isRemote)
			new ParticleEnergyFX(Minecraft.getMinecraft().world, x, y, z, tx, ty, tz, rf).spawn();
		else
			super.spawnEnergyFX(w, x, y, z, tx, ty, tz, rf);
	}

	@Override
	public void spawnLiquidFX(World w, double x, double y, double z, double tx, double ty, double tz, int count, int color, float scale, int extend)
	{
		if(w == null)
			return;
		if(w.isRemote)
			ParticleProxy_Client.queueParticleSpawn(new ParticleLiquid(w, x, y, z, tx, ty, tz, count, color, scale, extend));
		else
			super.spawnLiquidFX(w, x, y, z, tx, ty, tz, count, color, scale, extend);
	}

	@Override
	public void spawnLiquidTexturedFX(World w, double x, double y, double z, double tx, double ty, double tz, int count, FluidStack stack, float scale, int extend)
	{
		if(w == null)
			return;
		if(w.isRemote)
			ParticleProxy_Client.queueParticleSpawn(new ParticleLiquidTextured(w, x, y, z, tx, ty, tz, count, stack, scale, extend));
		else
			super.spawnLiquidTexturedFX(w, x, y, z, tx, ty, tz, count, stack, scale, extend);
	}

	@Override
	public void addParticleVortex(Vortex vortex)
	{
		if(vortex == null || vortex.getVortexStrength() == 0 || particleVortex.contains(vortex))
			return;
		Set particleVortex = new HashSet<>(ClientProxy.particleVortex);
		particleVortex.add(vortex);
		ClientProxy.particleVortex = particleVortex;
	}

	@Override
	public void removeParticleVortex(Vortex vortex)
	{
		if(vortex == null || vortex.getVortexStrength() == 0 || !particleVortex.contains(vortex))
			return;
		Set particleVortex = new HashSet<>(ClientProxy.particleVortex);
		particleVortex.remove(vortex);
		ClientProxy.particleVortex = particleVortex;
	}
}