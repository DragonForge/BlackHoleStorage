package org.zeith.holestorage.items;

import com.zeitheron.hammercore.utils.IRegisterListener;
import net.minecraft.item.Item;
import net.minecraftforge.oredict.OreDictionary;

public class ItemOreDict
		extends Item
		implements IRegisterListener
{
	public final String ore;

	public ItemOreDict(String ore)
	{
		this.ore = ore;
	}

	@Override
	public void onRegistered()
	{
		OreDictionary.registerOre(ore, this);
	}
}