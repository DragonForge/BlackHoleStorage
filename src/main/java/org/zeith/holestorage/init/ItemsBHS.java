package org.zeith.holestorage.init;

import net.minecraft.item.Item;
import org.zeith.holestorage.items.ItemOreDict;
import org.zeith.holestorage.items.ItemUnobtainable;
import org.zeith.holestorage.items.ItemWormholePearl;

public class ItemsBHS
{
	public static final Item FORMER_RING = new Item().setTranslationKey("former_ring");
	public static final Item DARK_MATTER = new ItemOreDict("matterDark").setTranslationKey("dark_matter");
	public static final Item ANTI_MATTER = new ItemOreDict("matterAnti").setTranslationKey("anti_matter");
	public static final Item WORMHOLE_PEARL = new ItemWormholePearl();
	public static final Item DESTABILIZED_DIAMOND = new Item().setTranslationKey("destabilized_diamond");
	public static final Item STABILIZED_ELECTRICAL_DIAMOND = new Item().setTranslationKey("stabilized_electrical_diamond");

	// UNOBTAINABLE ITEMS, USED FOR ITEM MODELS
	public static final Item BLACK_HOLE_SHIELD = new ItemUnobtainable("black_hole_shield");
	public static final Item ANTI_MATTER_CORE = new ItemUnobtainable("anti_matter_core");
	public static final Item ANTI_MATTER_POSITRON_1 = new ItemUnobtainable("anti_matter_positron_1");
	public static final Item ANTI_MATTER_POSITRON_2 = new ItemUnobtainable("anti_matter_positron_2");
	public static final Item ANTI_MATTER_POSITRON_3 = new ItemUnobtainable("anti_matter_positron_3");
}