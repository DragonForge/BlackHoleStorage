package org.zeith.holestorage.init;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.zeitheron.hammercore.lib.zlib.io.IOUtils;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.commons.io.FileUtils;
import org.zeith.holestorage.BlackHoleStorage;
import org.zeith.holestorage.InfoBHS;
import org.zeith.holestorage.api.atomictransformer.ATRecipe;
import org.zeith.holestorage.api.atomictransformer.RegisterATRecipesEvent;
import org.zeith.holestorage.configs.BHSConfigs;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Mod.EventBusSubscriber
public class RecipesBHS
{
	private static final List<ATRecipe> LOADED_RECIPES = new ArrayList<>();

	public static void reloadCustomRecipes()
	{
		File configDir = BHSConfigs.getConfigDir();
		File recipesDir = new File(configDir, "at_recipes");

		File defRecipes = new File(recipesDir, "default.json");

		boolean extract = !defRecipes.isFile();
		String currentVersion = InfoBHS.MOD_VERSION;

		if(defRecipes.isFile())
		{
			try
			{
				String version = new String((byte[]) Files.getAttribute(defRecipes.toPath(), "user:" + InfoBHS.MOD_ID));
				if(!version.equals(currentVersion))
				{
					extract = true;
					BlackHoleStorage.LOG.info("Default recipe version detected " + version + " does not match with current version " + currentVersion + "; Replacing recipe file.");
				}
			} catch(IOException e)
			{
				e.printStackTrace();
			}
		}

		if((!recipesDir.isDirectory() && recipesDir.mkdirs()) || extract)
		{
			backup:
			if(defRecipes.isFile())
			{
				File old = new File(recipesDir, "default.json.old");
				if(old.isFile())
				{
					try
					{
						FileUtils.forceDelete(old);
					} catch(IOException e)
					{
						e.printStackTrace();
						break backup;
					}
				}

				defRecipes.renameTo(old);
			}

			try(InputStream in = RecipesBHS.class.getResourceAsStream("/at_recipes.json"); FileOutputStream out = new FileOutputStream(defRecipes))
			{
				IOUtils.pipeData(in, out);
			} catch(IOException e)
			{
				e.printStackTrace();
			}

			try
			{
				Files.setAttribute(defRecipes.toPath(), "user:" + InfoBHS.MOD_ID, ByteBuffer.wrap(currentVersion.getBytes(StandardCharsets.UTF_8)));
			} catch(IOException e)
			{
				e.printStackTrace();
			}
		}

		LOADED_RECIPES.clear();

		File[] sub = recipesDir.listFiles(f -> f.getName().endsWith(".json"));
		JsonParser parser = new JsonParser();
		if(sub != null) for(File file : sub)
		{
			int prev = LOADED_RECIPES.size();

			try(JsonReader r = new JsonReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)))
			{
				addRecipeFromJson(parser.parse(r), LOADED_RECIPES::add);
			} catch(Exception e)
			{
				BlackHoleStorage.LOG.error("Failed to load AT recipe from " + file.getName(), e);
			}

			int added = LOADED_RECIPES.size() - prev;

			if(added > 0)
				BlackHoleStorage.LOG.info("Added {} recipe{} from {}", added, added != 1 ? "s" : "", file.getName());
		}
	}

	private static void addRecipeFromJson(JsonElement element, Consumer<ATRecipe> registrar) throws JsonSyntaxException
	{
		if(element.isJsonArray()) for(JsonElement e : element.getAsJsonArray()) addRecipeFromJson(e, registrar);
		else if(element.isJsonObject())
		{
			JsonObject obj = element.getAsJsonObject();
			registrar.accept(new ATRecipe(obj));
		} else throw new JsonSyntaxException("Unable to decode element " + element + " into AT recipe.");
	}

	@SubscribeEvent
	public static void registerAT(RegisterATRecipesEvent e)
	{
		reloadCustomRecipes();
		LOADED_RECIPES.forEach(e::register);
	}
}