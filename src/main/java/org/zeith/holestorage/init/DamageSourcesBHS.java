package org.zeith.holestorage.init;

import net.minecraft.util.DamageSource;
import org.zeith.holestorage.InfoBHS;

public class DamageSourcesBHS
{
	public static final DamageSource BLACK_HOLE = new DamageSource(InfoBHS.MOD_ID + ":black_hole");
}