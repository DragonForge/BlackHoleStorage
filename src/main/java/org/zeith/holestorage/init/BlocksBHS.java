package org.zeith.holestorage.init;

import net.minecraft.block.Block;
import org.zeith.holestorage.blocks.*;

public class BlocksBHS
{
	public static final Block //
	        BLACK_HOLE = new BlockBlackHole(), //
	        BLACK_HOLE_FORMER = new BlockBlackHoleFormer(), //
	        BLACK_HOLE_CHARGER = new BlockBlackHoleCharger(), //
	        RF_INJECTOR = new BlockRFInjector(), //
	        RF_EJECTOR = new BlockRFEjector(), //
	        FLUID_INJECTOR = new BlockFluidInjector(), //
	        FLUID_EJECTOR = new BlockFluidEjector(), //
	        ATOMIC_TRANSFORMER = new BlockAtomicTransformer(), //
	        WORMHOLE = new BlockWormhole(), //
	        WORMHOLE_FORMER = new BlockWormholeFormer();
}