package org.zeith.holestorage.api;

import com.zeitheron.hammercore.utils.base.Cast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class ListenerArrayList<T>
		extends ArrayList<T>
{
	final List<IListListener<T>> listeners = new ArrayList<>();

	public void addListener(IListListener<T> listener)
	{
		listeners.add(listener);
	}

	@Override
	public void clear()
	{
		for(T t : this) listeners.forEach(l -> l.onRemove(t));
		super.clear();
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		for(Object t : c) if(contains(t)) listeners.forEach(l -> l.onRemove(Cast.cast(t)));
		return super.removeAll(c);
	}

	@Override
	public boolean removeIf(Predicate<? super T> filter)
	{
		for(T t : this) if(filter.test(t)) listeners.forEach(l -> l.onRemove(t));
		return super.removeIf(filter);
	}

	@Override
	public boolean addAll(Collection<? extends T> c)
	{
		boolean a = super.addAll(c);
		c.forEach(t -> listeners.forEach(l -> l.onAdd(t)));
		return a;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c)
	{
		boolean a = super.addAll(index, c);
		c.forEach(t -> listeners.forEach(l -> l.onAdd(t)));
		return a;
	}

	@Override
	public boolean add(T t)
	{
		boolean a = super.add(t);
		listeners.forEach(l -> l.onAdd(t));
		return a;
	}

	@Override
	public void add(int index, T element)
	{
		super.add(index, element);
		listeners.forEach(l -> l.onAdd(element));
	}

	@Override
	public boolean remove(Object o)
	{
		listeners.forEach(l -> l.onRemove(Cast.cast(o)));
		return super.remove(o);
	}

	@Override
	public T remove(int index)
	{
		T r = super.remove(index);
		listeners.forEach(l -> l.onRemove(r));
		return r;
	}

	public interface IListListener<T>
	{
		void onAdd(T element);

		void onRemove(T element);
	}
}