package org.zeith.holestorage.api.atomictransformer;

import com.zeitheron.hammercore.internal.variables.VariableManager;
import com.zeitheron.hammercore.internal.variables.VariableRefreshEvent;
import com.zeitheron.hammercore.internal.variables.types.VariableCompoundNBT;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.base.SideLocal;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.zeith.holestorage.BlackHoleStorage;
import org.zeith.holestorage.InfoBHS;
import org.zeith.holestorage.api.ListenerArrayList;

import java.util.List;

@Mod.EventBusSubscriber
public class RecipesAtomicTransformer
{
	private static final VariableCompoundNBT ATOMIC_TRANSFORMER_RECIPES = new VariableCompoundNBT(new ResourceLocation(InfoBHS.MOD_ID, "at_recipes"));

	public static final SideLocal<ListenerArrayList<ATRecipe>> RECIPES = SideLocal.initializeForBoth(ListenerArrayList::new);

	@SubscribeEvent
	public static void reloadVariable(VariableRefreshEvent e)
	{
		if(e.getVariable() == ATOMIC_TRANSFORMER_RECIPES && FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
		{
			BlackHoleStorage.LOG.info("Accepting server AT recipes.");
			readFromVar();
		}
	}

	public static void init()
	{
		VariableManager.registerVariable(ATOMIC_TRANSFORMER_RECIPES);
	}

	public static void reload()
	{
		List<ATRecipe> recipeList = RecipesAtomicTransformer.RECIPES.get();

		recipeList.clear();
		MinecraftForge.EVENT_BUS.post(new RegisterATRecipesEvent(recipeList::add));
		recipeList.removeIf(ATRecipe::isInvalid);

		NBTTagCompound tag = new NBTTagCompound();
		{
			NBTTagList recipes = new NBTTagList();
			for(ATRecipe recipe : recipeList)
				recipes.appendTag(recipe.serializeNBT());
			tag.setTag("Recipes", recipes);
		}
		ATOMIC_TRANSFORMER_RECIPES.set(null);
		ATOMIC_TRANSFORMER_RECIPES.set(tag);
	}

	public static void readFromVar()
	{
		List<ATRecipe> recipeList = RecipesAtomicTransformer.RECIPES.get();
		recipeList.clear();

		NBTTagCompound nbt = ATOMIC_TRANSFORMER_RECIPES.get();
		{
			NBTTagList recipes = nbt.getTagList("Recipes", Constants.NBT.TAG_COMPOUND);

			for(int i = 0; i < recipes.tagCount(); ++i)
			{
				NBTTagCompound tag = recipes.getCompoundTagAt(i);
				ATRecipe r = new ATRecipe(tag);
				if(r.isValid()) recipeList.add(r);
			}
		}
	}

	public static List<ATRecipe> getRecipes()
	{
		return RECIPES.get();
	}

	public static ATRecipe getRecipeFor(ItemStack input)
	{
		if(InterItemStack.isStackNull(input))
			return null;
		for(ATRecipe r : getRecipes())
			if(r.matches(input))
				return r;
		return null;
	}

	public static void reset()
	{
		RECIPES.get(Side.CLIENT).clear();
		RECIPES.get(Side.SERVER).clear();
	}
}