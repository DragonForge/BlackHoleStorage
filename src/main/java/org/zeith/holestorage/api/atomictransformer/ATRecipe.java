package org.zeith.holestorage.api.atomictransformer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.zeitheron.hammercore.utils.InterItemStack;
import net.minecraft.client.util.RecipeItemHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.JsonContext;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.oredict.OreIngredient;
import org.zeith.holestorage.InfoBHS;

import java.math.BigInteger;
import java.util.Arrays;

public final class ATRecipe
		implements INBTSerializable<NBTTagCompound>
{
	public Ingredient input;
	public int itemCount;
	public BigInteger energy;
	public ItemStack result;

	private boolean isValid;

	public ATRecipe(NBTTagCompound nbt)
	{
		deserializeNBT(nbt);
	}

	public ATRecipe(Ingredient input, int itemCount, long energy, ItemStack result)
	{
		this(input, itemCount, BigInteger.valueOf(energy), result);
	}

	public ATRecipe(Ingredient input, int itemCount, BigInteger energy, ItemStack result)
	{
		this.input = input;
		this.itemCount = itemCount;
		this.energy = energy;
		this.result = result;
		this.isValid = true;
	}

	public ATRecipe(ItemStack in, ItemStack out, long energy)
	{
		this(in, out, BigInteger.valueOf(energy));
	}

	public ATRecipe(ItemStack in, ItemStack out, BigInteger energy)
	{
		this.input = Ingredient.fromStacks(in);
		this.itemCount = InterItemStack.getStackSize(in);
		this.result = out;
		this.energy = energy;
		this.isValid = true;
	}

	public ATRecipe(String in, ItemStack out, long energy)
	{
		this(in, out, BigInteger.valueOf(energy));
	}

	public ATRecipe(String in, ItemStack out, BigInteger energy)
	{
		this(in, 1, out, energy);
	}

	public ATRecipe(String in, int count, ItemStack out, long energy)
	{
		this(in, count, out, BigInteger.valueOf(energy));
	}

	public ATRecipe(String in, int count, ItemStack out, BigInteger energy)
	{
		this.input = new OreIngredient(in);
		this.itemCount = count;
		this.result = out;
		this.energy = energy;
		this.isValid = true;
	}

	public ATRecipe(JsonObject obj)
	{
		fromJson(obj);
	}

	public boolean matches(ItemStack input)
	{
		return this.input.test(input) && input.getCount() >= itemCount;
	}

	public ItemStack getOutput(ItemStack input)
	{
		return this.result.copy();
	}

	public ItemStack getResult()
	{
		return this.result.copy();
	}

	public BigInteger getEnergyUsed(ItemStack input)
	{
		return this.energy;
	}

	public int getInputItemCount()
	{
		return itemCount;
	}

	public Ingredient getInput()
	{
		return input;
	}

	public boolean isValid()
	{
		return isValid;
	}

	public boolean isInvalid()
	{
		return !isValid();
	}

	public void fromJson(JsonElement json)
	{
		JsonObject jo = json.getAsJsonObject();

		JsonContext ctx = new JsonContext(InfoBHS.MOD_ID);

		this.input = CraftingHelper.getIngredient(jo.get("input"), ctx);
		this.itemCount = jo.has("inputCount") ? jo.get("inputCount").getAsInt() : 1;
		this.energy = jo.get("energy").getAsBigInteger();
		this.result = CraftingHelper.getItemStack(jo.getAsJsonObject("result"), ctx);
		this.isValid = CraftingHelper.processConditions(jo, "conditions", ctx);
	}

	@Override
	public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();

		nbt.setIntArray("Input", this.input.getValidItemStacksPacked().toIntArray());
		nbt.setInteger("InputCount", this.itemCount);
		nbt.setString("Energy", this.energy.toString());
		nbt.setTag("Result", this.result.serializeNBT());

		return nbt;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		this.input = Ingredient.fromStacks(Arrays.stream(nbt.getIntArray("Input")).mapToObj(RecipeItemHelper::unpack).toArray(ItemStack[]::new));
		this.itemCount = nbt.getInteger("InputCount");
		this.energy = new BigInteger(nbt.getString("Energy"));
		this.result = new ItemStack(nbt.getCompoundTag("Result"));

		this.isValid = !this.input.getValidItemStacksPacked().isEmpty() && this.itemCount > 0 && !this.result.isEmpty();
	}
}