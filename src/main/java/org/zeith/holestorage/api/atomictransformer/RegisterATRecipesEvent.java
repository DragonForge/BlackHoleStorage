package org.zeith.holestorage.api.atomictransformer;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.Event;

import java.math.BigInteger;
import java.util.function.Consumer;

public class RegisterATRecipesEvent
		extends Event
{
	public final Consumer<ATRecipe> handler;

	public RegisterATRecipesEvent(Consumer<ATRecipe> handler)
	{
		this.handler = handler;
	}

	public void register(ATRecipe recipe)
	{
		handler.accept(recipe);
	}

	public void register(ItemStack in, ItemStack out, BigInteger rf)
	{
		register(new ATRecipe(in, out, rf));
	}

	public void register(ItemStack in, ItemStack out, long rf)
	{
		register(new ATRecipe(in, out, rf));
	}

	public void register(String in, ItemStack out, BigInteger rf)
	{
		register(new ATRecipe(in, out, rf));
	}

	public void register(String in, ItemStack out, long rf)
	{
		register(new ATRecipe(in, out, rf));
	}
}