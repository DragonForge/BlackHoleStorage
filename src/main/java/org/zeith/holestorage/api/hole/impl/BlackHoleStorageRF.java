package org.zeith.holestorage.api.hole.impl;

import net.minecraft.nbt.NBTTagCompound;
import org.zeith.holestorage.api.hole.ActionType;
import org.zeith.holestorage.api.hole.BlackHolePacket;
import org.zeith.holestorage.api.hole.BlackHolePacket.EnumBlackHolePacketType;
import org.zeith.holestorage.api.hole.IBlackHoleStorage;

import java.math.BigInteger;

public class BlackHoleStorageRF
		implements IBlackHoleStorage<BigInteger>
{
	private BigInteger stored = BigInteger.ZERO;

	@Override
	public boolean canHandle(BlackHolePacket<BigInteger, ?> pkt)
	{
		if(pkt == null || pkt.type != EnumBlackHolePacketType.RF)
			return false;
		return pkt.stored != null;
	}

	@Override
	public BigInteger receive(BlackHolePacket<BigInteger, ?> obj, ActionType action)
	{
		if(action.execute()) stored = stored.add(obj.stored);
		return obj.stored;
	}

	@Override
	public BigInteger send(BlackHolePacket<BigInteger, ?> obj, ActionType action)
	{
		BigInteger extracted = stored.min(obj.stored);
		if(action.execute()) stored = stored.subtract(extracted);
		return extracted;
	}

	@Override
	public BigInteger getStored()
	{
		return stored;
	}

	@Override
	public void setStored(BigInteger obj)
	{
		stored = obj;
	}

	@Override
	public BlackHolePacket<BigInteger, ?> create(BigInteger number)
	{
		return new BlackHolePacket<>(number, EnumBlackHolePacketType.RF);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("EnergyStored", stored != null ? stored.toString() : "0");
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		stored = new BigInteger(nbt.getString("EnergyStored"));
	}

	@Override
	public EnumBlackHolePacketType getType()
	{
		return EnumBlackHolePacketType.RF;
	}

	@Override
	public boolean isEmpty()
	{
		return stored == null || stored.toString().equals("0") || stored.toString().equals("-0" /* is this the case with BigInts ? */);
	}
}