package org.zeith.holestorage.api.hole;

import net.minecraft.nbt.NBTTagCompound;
import org.zeith.holestorage.api.hole.BlackHolePacket.EnumBlackHolePacketType;

import java.math.BigInteger;

public interface IBlackHoleStorage<H>
{
	boolean canHandle(BlackHolePacket<H, ?> pkt);

	H receive(BlackHolePacket<H, ?> obj, ActionType action);

	H send(BlackHolePacket<H, ?> obj, ActionType action);

	H getStored();

	void setStored(H obj);

	BlackHolePacket<H, ?> create(BigInteger number);

	EnumBlackHolePacketType getType();

	boolean isEmpty();

	void writeToNBT(NBTTagCompound nbt);

	void readFromNBT(NBTTagCompound nbt);
}