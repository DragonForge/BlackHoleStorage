package org.zeith.holestorage.api.hole;

public enum ActionType
{
	EXECUTE,
	SIMULATE;

	public boolean execute()
	{
		return this == EXECUTE;
	}

	public static ActionType simulate(boolean simulate)
	{
		return simulate ? SIMULATE : EXECUTE;
	}
}