package org.zeith.holestorage.api.hole;

import java.math.BigInteger;

public interface IBlackHole
{
	Object receiveContent(BlackHolePacket<?, ?> packet, ActionType action);

	Object sendContent(BigInteger amount, ActionType action);

	IBlackHoleStorage<?> getStorage();

	void setStorage(IBlackHoleStorage<?> storage);
}