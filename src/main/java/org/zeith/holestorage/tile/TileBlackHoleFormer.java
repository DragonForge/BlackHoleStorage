package org.zeith.holestorage.tile;

import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.ITooltipTile;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.BigIntegerUtils;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.holestorage.InfoBHS;
import org.zeith.holestorage.configs.BHSConfigs;
import org.zeith.holestorage.init.BlocksBHS;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class TileBlackHoleFormer
		extends TileSyncableTickable
		implements IEnergyStorage, ITooltipTile, ITooltipProviderHC
{
	public BigInteger EnergyStored = BigInteger.ZERO;
	public BigDecimal EnergyStoredD = BigDecimal.ZERO;

	public void setEnergy(BigInteger energy)
	{
		EnergyStored = energy;
		EnergyStoredD = new BigDecimal(energy);
	}

	@Override
	public void tick()
	{
		if(EnergyStored.max(BHSConfigs.getBHForgerAbsorbed()).equals(EnergyStored) && !world.isRemote)
		{
			world.setBlockState(pos.up(), BlocksBHS.BLACK_HOLE.getDefaultState());
			getLocation().destroyBlock(false);
			getLocation().playSound(InfoBHS.MOD_ID + ":black_hole_form", 4F, 1F, SoundCategory.BLOCKS);
			setEnergy(BigInteger.ZERO);
		}
	}
	
	@Override
	public int getEnergyStored()
	{
		return BigIntegerUtils.isInt(EnergyStored) ? EnergyStored.intValue() : Integer.MAX_VALUE;
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return Integer.MAX_VALUE;
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		BigInteger toConsume = BHSConfigs.getBHForgerAbsorbed().subtract(EnergyStored).min(BigInteger.valueOf(maxReceive));
		if(!simulate)
		{
			setEnergy(EnergyStored.add(toConsume));
			sync();
		}
		return toConsume.intValue();
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setString("EnergyStored", EnergyStored.toString());
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		setEnergy(new BigInteger(nbt.getString("EnergyStored")));
		setTooltipDirty(true);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void getTextTooltip(List<String> list, EntityPlayer player)
	{
		double progress = EnergyStored.divide(BHSConfigs.getBHForgerAbsorbed()).doubleValue() * 100D;
		list.add(I18n.format("gui." + InfoBHS.MOD_ID + ":progress") + ": " + String.format("%.002f", progress) + "%");
	}
	
	public boolean dirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return dirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		this.dirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		double progress = EnergyStoredD.divide(BHSConfigs.getBHForgerAbsorbedD()).doubleValue() * 100D;
		tip.append(new TranslationTooltipInfo("gui." + InfoBHS.MOD_ID + ":progress"));
		tip.append(new StringTooltipInfo(": " + String.format("%.002f", progress) + "%"));
	}
}