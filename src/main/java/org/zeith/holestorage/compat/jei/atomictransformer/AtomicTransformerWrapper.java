package org.zeith.holestorage.compat.jei.atomictransformer;

import com.zeitheron.hammercore.utils.InterItemStack;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.ingredients.VanillaTypes;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import org.zeith.holestorage.InfoBHS;
import org.zeith.holestorage.api.atomictransformer.ATRecipe;

import java.util.Collections;
import java.util.List;

public class AtomicTransformerWrapper
		implements IRecipeWrapper
{
	private final IJeiHelpers jeiHelpers;
	final ATRecipe recipe;

	public AtomicTransformerWrapper(IJeiHelpers jeiHelpers, ATRecipe recipe)
	{
		this.jeiHelpers = jeiHelpers;
		this.recipe = recipe;
	}

	@Override
	public void drawInfo(Minecraft mc, int arg1, int arg2, int arg3, int arg4)
	{
		FontRenderer fontRendererObj = mc.fontRenderer;
		GL11.glPushMatrix();
		GL11.glTranslated(44, 9, 0);
		GL11.glScaled(.5, .5, 1);
		fontRendererObj.drawString(I18n.format("gui." + InfoBHS.MOD_ID + ":rf.required") + ": " + String.format("%,d", recipe.getEnergyUsed(ItemStack.EMPTY)) + " RF", 0, 36, 0, false);
		fontRendererObj.drawString(I18n.format("gui." + InfoBHS.MOD_ID + ":output") + ": " + InterItemStack.getStackSize(recipe.getResult()) + "x " + recipe.getResult().getDisplayName(), 0, 54, 0, false);
		GL11.glPopMatrix();
	}

	@Override
	public void getIngredients(IIngredients ing)
	{
		List<List<ItemStack>> inputs = jeiHelpers.getStackHelper().expandRecipeItemStackInputs(Collections.singletonList(recipe.getInput()));
		inputs.get(0).forEach(s -> s.setCount(recipe.itemCount));
		ing.setInputLists(VanillaTypes.ITEM, inputs);

		ing.setOutput(VanillaTypes.ITEM, recipe.getResult());
	}

	@Override
	public List<String> getTooltipStrings(int arg0, int arg1)
	{
		return null;
	}

	@Override
	public boolean handleClick(Minecraft arg0, int arg1, int arg2, int arg3)
	{
		return false;
	}
}