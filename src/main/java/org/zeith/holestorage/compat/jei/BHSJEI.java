package org.zeith.holestorage.compat.jei;

import mezz.jei.api.*;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import org.zeith.holestorage.InfoBHS;
import org.zeith.holestorage.api.ListenerArrayList;
import org.zeith.holestorage.api.atomictransformer.ATRecipe;
import org.zeith.holestorage.api.atomictransformer.RecipesAtomicTransformer;
import org.zeith.holestorage.compat.jei.atomictransformer.AtomicTransformerCategory;
import org.zeith.holestorage.compat.jei.atomictransformer.AtomicTransformerWrapper;
import org.zeith.holestorage.gui.ui.GuiAtomicTransformer;
import org.zeith.holestorage.init.BlocksBHS;

import java.util.ArrayList;
import java.util.List;

@JEIPlugin
public class BHSJEI
		implements IModPlugin
{
	public static final String ATOMIC_TRANSFORMER = InfoBHS.MOD_ID + ":atomic_transformer";

	final List<ATRecipe> at2add = new ArrayList<>(), at2remove = new ArrayList<>();

	public BHSJEI()
	{
		RecipesAtomicTransformer.RECIPES.get(Side.CLIENT).addListener(new ListenerArrayList.IListListener<ATRecipe>()
		{
			@Override
			public void onAdd(ATRecipe element)
			{
				if(jei != null)
					Minecraft.getMinecraft().addScheduledTask(() ->
					{
						IRecipeRegistry reg = jei.getRecipeRegistry();
						IRecipeWrapper wra = reg.getRecipeWrapper(element, ATOMIC_TRANSFORMER);
						if(wra != null) reg.addRecipe(wra, ATOMIC_TRANSFORMER);
					});
				else at2add.add(element);
			}

			@Override
			public void onRemove(ATRecipe element)
			{
				if(jei != null)
					Minecraft.getMinecraft().addScheduledTask(() ->
					{
						IRecipeRegistry reg = jei.getRecipeRegistry();
						IRecipeWrapper wra = reg.getRecipeWrapper(element, ATOMIC_TRANSFORMER);
						if(wra != null) reg.hideRecipe(wra, ATOMIC_TRANSFORMER);
					});
				else at2remove.add(element);
			}
		});
	}

	@Override
	public void register(IModRegistry reg)
	{
		IJeiHelpers jeiHelpers = reg.getJeiHelpers();

		reg.handleRecipes(ATRecipe.class, r -> new AtomicTransformerWrapper(jeiHelpers, r), ATOMIC_TRANSFORMER);

		reg.addRecipeCatalyst(new ItemStack(BlocksBHS.ATOMIC_TRANSFORMER), ATOMIC_TRANSFORMER);

		reg.addRecipeClickArea(GuiAtomicTransformer.class, 17, 30, 16, 26, ATOMIC_TRANSFORMER);
	}

	@Override
	public void registerCategories(IRecipeCategoryRegistration reg)
	{
		IJeiHelpers jeiHelpers = reg.getJeiHelpers();

		reg.addRecipeCategories(new AtomicTransformerCategory(jeiHelpers.getGuiHelper()));
	}

	public static IJeiRuntime jei;

	@Override
	public void onRuntimeAvailable(IJeiRuntime jeiRuntime)
	{
		jei = jeiRuntime;

		IRecipeRegistry rr = jei.getRecipeRegistry();
		at2remove.forEach(rr::removeRecipe);
		at2add.forEach(rr::addRecipe);

		at2remove.clear();
		at2add.clear();
	}
}