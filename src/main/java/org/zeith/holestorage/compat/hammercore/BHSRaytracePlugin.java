package org.zeith.holestorage.compat.hammercore;

import com.zeitheron.hammercore.api.mhb.BlockTraceable;
import com.zeitheron.hammercore.api.mhb.IRayCubeRegistry;
import com.zeitheron.hammercore.api.mhb.IRayRegistry;
import com.zeitheron.hammercore.api.mhb.RaytracePlugin;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import org.zeith.holestorage.init.BlocksBHS;

@RaytracePlugin
public class BHSRaytracePlugin implements IRayRegistry
{
	@Override
	public void registerCubes(IRayCubeRegistry cube)
	{
		cube.bindBlockCube6((BlockTraceable) BlocksBHS.WORMHOLE_FORMER, new Cuboid6(0, 0, 0, 1, .5, 1), new Cuboid6(0, .5, 0, 1, 1, 1));
	}
}