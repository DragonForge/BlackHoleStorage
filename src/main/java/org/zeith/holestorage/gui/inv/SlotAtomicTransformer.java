package org.zeith.holestorage.gui.inv;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import org.zeith.holestorage.api.atomictransformer.RecipesAtomicTransformer;

public class SlotAtomicTransformer extends Slot
{
	public SlotAtomicTransformer(IInventory inventoryIn, int index, int xPosition, int yPosition)
	{
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return RecipesAtomicTransformer.getRecipeFor(stack) != null;
	}
}