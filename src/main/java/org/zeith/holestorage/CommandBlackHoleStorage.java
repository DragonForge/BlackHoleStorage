package org.zeith.holestorage;

import net.minecraft.command.ICommandSender;
import net.minecraftforge.server.command.CommandTreeBase;
import org.zeith.holestorage.commands.ReloadAT;

public class CommandBlackHoleStorage
		extends CommandTreeBase
{
	public CommandBlackHoleStorage()
	{
		addSubcommand(new ReloadAT());
	}

	@Override
	public String getName()
	{
		return InfoBHS.MOD_ID;
	}

	@Override
	public String getUsage(ICommandSender sender)
	{
		return "Mod's command root.";
	}
}