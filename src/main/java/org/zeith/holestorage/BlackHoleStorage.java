package org.zeith.holestorage;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zeith.holestorage.api.atomictransformer.RecipesAtomicTransformer;
import org.zeith.holestorage.configs.BHSConfigs;
import org.zeith.holestorage.init.BlocksBHS;
import org.zeith.holestorage.init.ItemsBHS;
import org.zeith.holestorage.init.ManualBHS;
import org.zeith.holestorage.proxy.CommonProxy;
import org.zeith.holestorage.tabs.CreativeTabBlackHoleStorage;

import java.io.File;

@Mod(modid = InfoBHS.MOD_ID, version = InfoBHS.MOD_VERSION, name = InfoBHS.MOD_NAME, dependencies = "required-after:hammercore", guiFactory = "org.zeith.holestorage.configs.BHSConfigFactory", certificateFingerprint = "9f5e2a811a8332a842b34f6967b7db0ac4f24856", updateJSON = "http://dccg.herokuapp.com/api/fmluc/261061")
public class BlackHoleStorage
{
	@SidedProxy(clientSide = InfoBHS.PROXY_CLIENT, serverSide = InfoBHS.PROXY_SERVER)
	public static CommonProxy proxy;

	@Instance
	public static BlackHoleStorage instance;

	public static File cfgFolder;

	public static final Logger LOG = LogManager.getLogger(InfoBHS.MOD_ID);

	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with BlackHoleStorage jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://www.curseforge.com/projects/261061 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put(InfoBHS.MOD_ID, "https://www.curseforge.com/projects/261061");
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		String cfg = event.getSuggestedConfigurationFile().getAbsolutePath();
		cfg = cfg.substring(0, cfg.lastIndexOf("."));
		cfgFolder = new File(cfg);
		cfgFolder.mkdirs();

		RecipesAtomicTransformer.init();
		BHSConfigs.init();

		proxy.preInit();
		SimpleRegistration.registerFieldBlocksFrom(BlocksBHS.class, InfoBHS.MOD_ID, CreativeTabBlackHoleStorage.BLACK_HOLE_STORAGE);
		SimpleRegistration.registerFieldItemsFrom(ItemsBHS.class, InfoBHS.MOD_ID, CreativeTabBlackHoleStorage.BLACK_HOLE_STORAGE);
	}

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(proxy);
		proxy.init();
		ManualBHS.load();
	}

	@EventHandler
	public void startServer(FMLServerStartingEvent event)
	{
		event.registerServerCommand(new CommandBlackHoleStorage());
		RecipesAtomicTransformer.reload();
		BHSConfigs.reloadNetConfigs();
	}

	@EventHandler
	public void setopServer(FMLServerStoppingEvent event)
	{
		RecipesAtomicTransformer.reset();
	}
}