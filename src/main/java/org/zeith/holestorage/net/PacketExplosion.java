package org.zeith.holestorage.net;

import com.zeitheron.hammercore.api.IProcess;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.holestorage.client.particle.ParticleExplosion;

public class PacketExplosion implements IPacket
{
	public long pos;
	
	static
	{
		IPacket.handle(PacketExplosion.class, PacketExplosion::new);
	}
	
	public PacketExplosion(long pos)
	{
		this.pos = pos;
	}
	
	public PacketExplosion()
	{
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("p", pos);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		pos = nbt.getLong("p");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		BlockPos pos = BlockPos.fromLong(this.pos);
		new IProcess()
		{
			public int ticks = 0;
			
			@Override
			public void update()
			{
				++ticks;
				if(ticks == 1)
					ParticleProxy_Client.queueParticleSpawn(new ParticleExplosion(Minecraft.getMinecraft().world, pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5));
			}
			
			@Override
			public boolean isAlive()
			{
				return ticks <= 0;
			}
		}.start();
		return null;
	}
}