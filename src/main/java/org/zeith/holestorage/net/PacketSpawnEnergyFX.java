package org.zeith.holestorage.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.holestorage.BlackHoleStorage;

public class PacketSpawnEnergyFX implements IPacket
{
	private int dim, rf;
	private double x, y, z, tx, ty, tz;
	
	static
	{
		IPacket.handle(PacketSpawnEnergyFX.class, PacketSpawnEnergyFX::new);
	}
	
	public PacketSpawnEnergyFX()
	{
	}
	
	public PacketSpawnEnergyFX(int dim, double x, double y, double z, double tx, double ty, double tz, int rf)
	{
		this.dim = dim;
		this.x = x;
		this.y = y;
		this.z = z;
		this.tx = tx;
		this.ty = ty;
		this.tz = tz;
		this.rf = rf;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("p1", dim);
		nbt.setDouble("p2", x);
		nbt.setDouble("p3", y);
		nbt.setDouble("p4", z);
		nbt.setDouble("p5", tx);
		nbt.setDouble("p6", ty);
		nbt.setDouble("p7", tz);
		nbt.setInteger("p8", rf);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		dim = nbt.getInteger("p1");
		x = nbt.getDouble("p2");
		y = nbt.getDouble("p3");
		z = nbt.getDouble("p4");
		tx = nbt.getDouble("p5");
		ty = nbt.getDouble("p6");
		tz = nbt.getDouble("p7");
		rf = nbt.getInteger("p8");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		BlackHoleStorage.proxy.spawnEnergyFX(WorldUtil.getWorld(net, dim), x, y, z, tx, ty, tz, rf);
		return null;
	}
}