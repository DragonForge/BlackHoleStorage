package org.zeith.holestorage.client.shaders;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.client.utils.gl.shading.InitializeShadersEvent;
import com.zeitheron.hammercore.client.utils.gl.shading.ShaderSource;
import com.zeitheron.hammercore.client.utils.gl.shading.VariableShaderProgram;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.holestorage.configs.BHSConfigs;

@SideOnly(Side.CLIENT)
@Mod.EventBusSubscriber(Side.CLIENT)
public class GLSLBlackHole
{
	private static VariableShaderProgram shader;

	@SubscribeEvent
	public static void init(InitializeShadersEvent e)
	{
		shader = new VariableShaderProgram()
				.id(new ResourceLocation("blackholestorage", "black_hole"))
				.linkFragmentSource(new ShaderSource(new ResourceLocation("blackholestorage", "shaders/black_hole.fsh")))
				.linkVertexSource(new ShaderSource(new ResourceLocation("blackholestorage", "shaders/black_hole.vsh")))
				.onCompilationFailed(VariableShaderProgram.ToastCompilationErrorHandler.INSTANCE)
				.onBind(GLSLBlackHole::uniforms)
				.doGLLog(false)
				.subscribe4Events();
		HammerCore.LOG.info("Created Ender Field GLSL Shader.");
	}

	public static VariableShaderProgram getShader()
	{
		return shader;
	}

	private static void uniforms(VariableShaderProgram program)
	{
		program.setUniform("time", (float) (Minecraft.getSystemTime() / 50000D));
	}

	public static boolean useShaders()
	{
		return OpenGlHelper.shadersSupported && BHSConfigs.client_useShaders;
	}
}