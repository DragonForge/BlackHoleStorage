package org.zeith.holestorage.client.tesr;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.utils.gl.shading.VariableShaderProgram;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.zeith.holestorage.client.shaders.GLSLBlackHole;
import org.zeith.holestorage.init.BlocksBHS;
import org.zeith.holestorage.init.ItemsBHS;
import org.zeith.holestorage.tile.TileBlackHole;

public class TileRenderBlackHole
		extends TESR<TileBlackHole>
{
	private ItemStack renderStack = ItemStack.EMPTY, shieldStack = ItemStack.EMPTY;

	private void makeStack()
	{
		renderStack = new ItemStack(BlocksBHS.BLACK_HOLE);
		shieldStack = new ItemStack(ItemsBHS.BLACK_HOLE_SHIELD);
	}

	@Override
	public void renderTileEntityAt(TileBlackHole te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		GL11.glPushMatrix();

		if(renderStack == null || renderStack.isEmpty() || renderStack.getItem() != Item.getItemFromBlock(BlocksBHS.BLACK_HOLE))
			makeStack();
		if(shieldStack == null || shieldStack.isEmpty() || shieldStack.getItem() != ItemsBHS.BLACK_HOLE_SHIELD)
			makeStack();

		GL11.glPushMatrix();
		GL11.glTranslated(x, y, z);

		RenderItem itemRender = Minecraft.getMinecraft().getRenderItem();

		double scale = 1.7 + Math.sqrt(te.additionalMass);

		double blackHoleRotationSpeed = (1 - te.currentShieldLevel);

		GL11.glTranslated(.5, .55 + -0.2 * scale, .5);
		GL11.glRotated((System.currentTimeMillis() / 500D % 10000D) * -360 * (1 - te.currentShieldLevel + .005F), 0, 1, 0);
		GL11.glScaled(scale, scale, scale);

		itemRender.renderItem(renderStack, TransformType.GROUND);

		GL11.glPopMatrix();

		if(!GLSLBlackHole.useShaders())
			return; // if we dont use shaders, dont render anything

		scale = scale * 1.06 * 1.15 + Math.sin(System.currentTimeMillis() / 25000D) * .1D;

		GL11.glTranslated(x + .5, y + .55 + -0.2 * scale, z + .5);
		GL11.glRotated((System.currentTimeMillis() / 500D % 10000D) * -360 * 8 * (te.currentShieldLevel + .005F), 0, 1, 0);
		GL11.glScaled(scale, scale, scale);

		double offsetConst = .125;
		GL11.glPushMatrix();

		GL11.glTranslated(offsetConst, offsetConst, offsetConst);

		VariableShaderProgram prog = GLSLBlackHole.getShader();
		prog.bindShader();
		prog.setUniform("shield", te.currentShieldLevel);
		itemRender.renderItem(shieldStack, TransformType.GROUND);
		prog.unbindShader();

		GL11.glPopMatrix();

		GL11.glPopMatrix();
	}
}