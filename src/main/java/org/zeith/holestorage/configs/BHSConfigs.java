package org.zeith.holestorage.configs;

import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyFloat;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyInt;
import com.zeitheron.hammercore.internal.variables.VariableManager;
import com.zeitheron.hammercore.internal.variables.VariableRefreshEvent;
import com.zeitheron.hammercore.internal.variables.types.VariableCompoundNBT;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.zeith.holestorage.BlackHoleStorage;
import org.zeith.holestorage.InfoBHS;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;

@Mod.EventBusSubscriber
@HCModConfigurations(modid = InfoBHS.MOD_ID, module = "main", isModule = true)
public class BHSConfigs
		implements IConfigReloadListener
{
	private static final VariableCompoundNBT MOD_CONFIGS = new VariableCompoundNBT(new ResourceLocation(InfoBHS.MOD_ID, "configs"));

	public static BHSConfigs instance;
	public static Configuration cfgs;

	@ModConfigPropertyBool(name = "Use Shaders", category = "Client", defaultValue = true, comment = "Set this to true to use shaders for rendering black hole's shield.\nIf your PC doesn't support shaders, this option will be ignored and you won't be able to see the shield levels, sorry :(")
	public static boolean client_useShaders = true;

	@ModConfigPropertyBool(name = "Use Particle Vortex", category = "Client", defaultValue = true, comment = "Set this to true to enable particle vortexes. This lets particles to be sucked inside the black hole, similar to how entities do.")
	public static boolean client_useParticleVortex = true;

	@ModConfigPropertyInt(name = "Seethrought Wormholes", category = "Client", defaultValue = 2, min = 0, max = 256, comment = "Set this to 0 to disable experimental seethrought feature for wormholes. Depending on your PC set this value between 1 and 256.\nThis value will change the framerate for wormhole.\nFor now, this feature doesn't work on wormholes that are far away or in the other dimension. Sorry!")
	public static int client_seeThroughtWormholes = 5;

	@ModConfigPropertyInt(name = "Wormhole FOV", category = "Client", defaultValue = 80, min = 10, max = 110, comment = "This property changes FOV of your wormhole. I don't recomend changing it, but feel free to do so if you want.")
	public static int client_wormholeFov = 80;

	@ModConfigPropertyInt(name = "Wormhole Quality", category = "Client", defaultValue = 128, min = 8, max = 1024, comment = "This property makes the quality of seethrought feature either more detailed or simplified. Higher values will decrease performance.")
	public static int client_wormholeQuality = 128;

	@ModConfigPropertyFloat(name = "Anti-Explosion Multiplier", category = "Black Hole", defaultValue = 1, min = 0, max = 2, comment = "Use this parameter to manipulate black hole's explosion.\nYou can't really fully control explosion power because it's calculated dynamically.\nSet this to 0 to disable explosion.\nSet this to 2 to make the explosion 2 times stronger.")
	public static float blackHole_explosionMultiplier = 1;

	@ModConfigPropertyInt(name = "Shield Consumption", category = "Black Hole", defaultValue = 200, min = 1, max = Integer.MAX_VALUE, comment = "How much energy (per tick) should be consumed by the black hole to keep it's shield stable?\nThe actual energy amount will be calculated by the following formula: FE/tick = ShieldConsumption + ShieldMassMultiplier * ({AdditionalMass} ^ ShieldMassPower)\nThis does not affect how fast shields charge the black hole, this just means how fast the shield's energy will be drained.")
	public static int blackHole_shieldConsumption = 200;

	@ModConfigPropertyFloat(name = "Shield Mass Power", category = "Black Hole", defaultValue = 1 / 4F, min = 0.00000001F, max = Float.MAX_VALUE, comment = "The power of additional mass, used for calculating FE/tick consumed by the black hole to keep the shield stable. Refer to \"Shield Consumption\"")
	public static float blackHole_massPower = 1 / 4F;

	@ModConfigPropertyFloat(name = "Shield Mass Multiplier", category = "Black Hole", defaultValue = 8F, min = 0F, max = Float.MAX_VALUE, comment = "The final multiplier for additional mass, raised to the power of \"Shield Mass Power\", used for calculating FE/tick consumed by the black hole to keep the shield stable. Refer to \"Shield Consumption\"")
	public static float blackHole_massMultiplier = 8F;

	private static BigInteger BH_FORMER_ABSORBED = BigInteger.valueOf(32_000_000_000L);
	private static BigDecimal BH_FORMER_ABSORBED_D = new BigDecimal(BH_FORMER_ABSORBED);

	public static void init()
	{
		VariableManager.registerVariable(MOD_CONFIGS);
	}


	public static BigInteger getBHForgerAbsorbed()
	{
		if(client != null) return client.bhFormer;
		return BH_FORMER_ABSORBED;
	}

	public static BigDecimal getBHForgerAbsorbedD()
	{
		if(client != null) return client.bhFormerD;
		return BH_FORMER_ABSORBED_D;
	}


	{
		instance = this;
	}

	private static ClientOverride client;

	public static void resetClient()
	{
		client = null;
	}

	@Override
	public void reloadCustom(Configuration cfgs)
	{
		BHSConfigs.cfgs = cfgs;
		BH_FORMER_ABSORBED = new BigInteger(cfgs.getString("Energy", "Black Hole Former", "32000000000", "How much energy do you need to create a black hole?"));
		BH_FORMER_ABSORBED_D = new BigDecimal(BH_FORMER_ABSORBED);
	}

	public static void reloadNetConfigs()
	{
		MOD_CONFIGS.set(null);
		NBTTagCompound tag = new NBTTagCompound();
		tag.setString("BHFormer", BH_FORMER_ABSORBED.toString());
		MOD_CONFIGS.set(tag);
	}

	@SubscribeEvent
	public static void reloadVariable(VariableRefreshEvent e)
	{
		if(e.getVariable() == MOD_CONFIGS && FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
		{
			BlackHoleStorage.LOG.info("Accepting server configs.");
			client = new ClientOverride(MOD_CONFIGS.get());
		}
	}

	public static File getConfigDir()
	{
		File f = new File(Loader.instance().getConfigDir(), InfoBHS.MOD_ID);
		if(!f.isDirectory())
			f.mkdirs();
		return f;
	}

	private static class ClientOverride
	{
		BigInteger bhFormer;
		BigDecimal bhFormerD;

		public ClientOverride(NBTTagCompound nbt)
		{
			bhFormer = new BigInteger(nbt.getString("BHFormer"));
			bhFormerD = new BigDecimal(bhFormer);
		}
	}
}