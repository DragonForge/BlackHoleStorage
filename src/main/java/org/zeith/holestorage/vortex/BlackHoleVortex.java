package org.zeith.holestorage.vortex;

import com.zeitheron.hammercore.client.particle.api.ParticleList;
import com.zeitheron.hammercore.proxy.ParticleProxy_Client;
import com.zeitheron.hammercore.utils.math.MathHelper;
import net.minecraft.client.particle.Particle;
import org.zeith.holestorage.BlackHoleStorage;
import org.zeith.holestorage.client.particle.ParticleEnergyFX;
import org.zeith.holestorage.client.particle.ParticleGeneric;
import org.zeith.holestorage.client.particle.ParticleLiquid;
import org.zeith.holestorage.client.particle.ParticleLiquidTextured;
import org.zeith.holestorage.configs.BHSConfigs;
import org.zeith.holestorage.tile.TileBlackHole;

public class BlackHoleVortex
		extends Vortex
{
	public final TileBlackHole tile;

	public BlackHoleVortex(TileBlackHole tile)
	{
		super(tile.getPos().getX() + .5, tile.getPos().getY() + .5, tile.getPos().getZ() + .5, 8, false);
		this.tile = tile;
	}

	@Override
	public void update()
	{
		if(tile == null || tile.getWorld().getTileEntity(tile.getPos()) != tile)
		{
			BlackHoleStorage.proxy.removeParticleVortex(this);
			return;
		}

		boolean isSlower = !tile.canPull;
		radius = isSlower ? 16 : tile.radius;

		x = tile.getPos().getX() + .5;
		y = tile.getPos().getY() + .5;
		z = tile.getPos().getZ() + .5;

		if(BHSConfigs.client_useParticleVortex)
			for(Particle p : ParticleList.getParticlesWithinAABB(getBoundingBox()))
			{
				if(p instanceof ParticleLiquid || p instanceof ParticleLiquidTextured || p instanceof ParticleEnergyFX || p instanceof ParticleGeneric)
					continue;

				double mx = ParticleProxy_Client.getParticleMotionX(p);
				double my = ParticleProxy_Client.getParticleMotionY(p);
				double mz = ParticleProxy_Client.getParticleMotionZ(p);

				double px = ParticleProxy_Client.getParticlePosX(p);
				double py = ParticleProxy_Client.getParticlePosY(p);
				double pz = ParticleProxy_Client.getParticlePosZ(p);

				double distSq = (tile.getPos().getX() - px + .5) * (tile.getPos().getX() - px + .5) + (tile.getPos().getY() - py + .5) * (tile.getPos().getY() - py + .5) + (tile.getPos().getZ() - pz + .5) * (tile.getPos().getZ() - pz + .5);
				double div = isSlower ? 128D : 32D;

				div /= Math.sqrt(distSq);

				mx += MathHelper.clip((x - px), -1, 1) / div;
				my += MathHelper.clip((y - py), -1, 1) / div;
				mz += MathHelper.clip((z - pz), -1, 1) / div;

				ParticleProxy_Client.setParticleMotionX(p, mx);
				ParticleProxy_Client.setParticleMotionY(p, my);
				ParticleProxy_Client.setParticleMotionZ(p, mz);

				if(distSq < Math.sqrt(tile.additionalMass) * 2 + .05)
					p.setExpired();
			}
	}
}