public class TestCalcSpeed
{
	public static void main(String[] args)
	{
		for(int x = 0; x < 128; ++x)
			System.out.println(x + ": " + Math.sqrt(Math.sqrt(x)) + " | " + Math.pow(x, 1 / 4D));
	}
}